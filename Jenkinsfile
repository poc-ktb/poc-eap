def mvnCmd = "mvn -s artifact_setting.xml" 
pipeline {
  agent {
      label 'maven-with-tools'
  }
  
  stages {

    stage('Pull Code') {
      steps {
        git branch: 'master', url: 'https://gitlab.com/poc-ktb/poc-eap.git'
      }
    }

    stage('Build App'){
      steps {
        sh "${mvnCmd} package -DskipTests=true" 
      }
    }

    stage('Unit Test'){
      steps {
        sh "${mvnCmd} test"
      }
    }

    stage('Scan Code'){
      steps {
        sh "echo sonar-scanner ..."
        withSonarQubeEnv('sonar-aws') {
            sh "${mvnCmd} sonar:sonar"
        }
        sleep(10)
        waitForQualityGate abortPipeline: true        
      }
    }
    
    stage('Push to Registry') {
      steps{
        sh "${mvnCmd} deploy -DskipTests=true -DaltDeploymentRepository=central::default::http://ec2-52-53-39-8.us-west-1.compute.amazonaws.com:8081/artifactory/libs-release/"
      }
    }
        
    stage('Release Approval') {
      steps {
        timeout(5) {
          input 'Should we deploy to Production?'
        }
      }
    }

    stage('Deploy War') {
      steps {
        withCredentials([usernamePassword(credentialsId: 'jboss-aws', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
          sh "/opt/jboss/bin/jboss-cli.sh --connect --controller=ec2-13-52-50-90.us-west-1.compute.amazonaws.com:9990 --user=${USERNAME} --password=${PASSWORD} --command='deploy target/poc-eap.war' "
        }
      }
    }
  }
}
